//
//  HWConverterViewController.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWConverterViewController.h"
#import "HWConverterView.h"

@interface HWConverterViewController () <UITextFieldDelegate>

@property (nonatomic, strong) HWConverterView *converterView;
@property (nonatomic, strong) NSRegularExpression *validInputRegularExpression;

@end

@implementation HWConverterViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSError *error = nil;
        _validInputRegularExpression = [[NSRegularExpression alloc] initWithPattern:@"^[0-9]*(.[0-9]*)?$"
                                                                            options:NSRegularExpressionAnchorsMatchLines
                                                                              error:&error];
        if (error) {
            NSLog(@"Error on creation regular expression: %@", error.description);
        }
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadView {
    self.converterView = [[HWConverterView alloc] init];
    self.view = self.converterView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.converterView.convertButton.enabled = [self isInputValid];
    
    self.converterView.inputTextField.delegate = self;
    [self.converterView.convertButton addTarget:self
                                         action:@selector(convertButtonEventHandler:)
                               forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldTextDidChangedNotificationHandler:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.converterView.inputTextField];
}

#pragma mark - Public methods

- (void)setConvertedString:(NSString *)convertedString {
    if (!convertedString) {
        convertedString = NSLocalizedString(@"converter.empty.text", nil);
    }
    self.converterView.resultLabel.text = convertedString;
    [self.converterView setNeedsLayout];
}

#pragma mark - Private methods

- (void)convertButtonEventHandler:(UIButton *)button {
    if (self.delegate) {
        [self.delegate converterController:self needConvertString:self.converterView.inputTextField.text];
    }
    [self.converterView endEditing:NO];
}

- (BOOL)isInputValid {
    return self.converterView.inputTextField.text.length > 0;
}

- (void)textFieldTextDidChangedNotificationHandler:(NSNotification *)notification {
    self.converterView.convertButton.enabled = [self isInputValid];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSRange resultRange = [_validInputRegularExpression rangeOfFirstMatchInString:resultString
                                                                    options:NSMatchingReportCompletion | NSMatchingAnchored
                                                                      range:NSMakeRange(0, resultString.length)];
    return resultRange.location != NSNotFound;
}

@end
