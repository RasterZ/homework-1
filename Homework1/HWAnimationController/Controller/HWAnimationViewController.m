//
//  HWAnimationViewController.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 28/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWAnimationViewController.h"
#import "HWAnimationView.h"

@interface HWAnimationViewController ()

@property (nonatomic, strong) HWAnimationView *animationView;

@property (nonatomic, strong) CADisplayLink *displayLink;

@property (nonatomic, assign) BOOL shouldResetTime;
@property (nonatomic, assign) NSTimeInterval displayLinkStartTime;

@end

@implementation HWAnimationViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _shouldResetTime = YES;
        _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displaplayLinkUpdated:)];
    }
    return self;
}

- (void)loadView {
    self.animationView = [[HWAnimationView alloc] init];
    self.view = self.animationView;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.displayLink removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}

#pragma mark - Public methods

- (void)addPoint:(CGPoint)point {
    [self.animationView addPoint:point];
}

#pragma mark - Private methods

- (void)displaplayLinkUpdated:(CADisplayLink *)displayLink {
    if (self.shouldResetTime) {
        self.displayLinkStartTime = displayLink.timestamp;
        self.shouldResetTime = NO;
    }
    if (self.delegate) {
        [self.delegate animationController:self needPointForAnimationTime:displayLink.timestamp - self.displayLinkStartTime];
    }
}

@end
